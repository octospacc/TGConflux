#!/usr/bin/env python3
from pyrogram import Client
from Config import *

def LoadRoutines():
	try:
		with open('Routines.txt', 'r') as f:
			Routines = f.read()
	except:
		print("Can't load Routines.txt (Error or it doesn't exist)")
		Routines = ""
	return Routines

def ParseRoutines(DB):
	RawRoutines = DB.split('\n\n')
	Routines = []
	for Sub in RawRoutines:
		Subroutine = {'TGIn':[], 'TGOut':[], 'MXIn':[], 'MXOut':[]}
		Sub0 = Sub.split('\n')
		for Elem in Sub0:
			Key = Elem.split(' ')[0]
			Keys = Elem.split(' ')[1:]
			Values = []
			for i in Keys:
				Values += [i]
			Subroutine[Key] = Values
		Routines += [Subroutine]
	return Routines

def IsStrInt(Str):
	try:
		int(Str)
		return True
	except ValueError:
		return False

def Main():
	App = Client("TGConflux", TGApiID, TGApiHash)
	Routines = ParseRoutines(LoadRoutines())

	@App.on_message()
	def message(Client, Msg):
		print(Msg)
		for Sub in Routines:
			if Msg.chat.id in Sub['TGIn'] or Msg.chat.username in Sub['TGIn']:
				for Chat in Sub['TGOut']:
					Target = int(Chat) if IsStrInt(Chat) else Chat
					App.send_message(Target, "From {0}\n[{1}:@{2}]".format(Msg.chat.title or Msg.chat.first_name, Msg.chat.id, Msg.chat.username))
					Msg.forward(Target)
			#elif Msg.chat.id in Sub['TGOut'] or Msg.chat.username in Sub['TGIn']:
			#	if Msg.from_user.id in Admins or Msg.from_user.username in Admins:
					

	App.run()

if __name__ == '__main__':
	Main()
